package com.sapient.ecommerce.model;

public enum OrderStatus {
	PENDING,
    PAID,
    PAYMENTFAILED,
    READY_FOR_DELIVERY,
    DELIVERED
}
