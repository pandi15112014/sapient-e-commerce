package com.sapient.ecommerce.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sapient.ecommerce.dto.OrderProductDto;
import com.sapient.ecommerce.exception.ResourceNotFoundException;
import com.sapient.ecommerce.model.Order;
import com.sapient.ecommerce.model.OrderProduct;
import com.sapient.ecommerce.model.OrderStatus;
import com.sapient.ecommerce.model.Product;
import com.sapient.ecommerce.service.OrderProductService;
import com.sapient.ecommerce.service.OrderService;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

	@Autowired
	RestTemplate restTemplate;
	OrderService orderService;
	OrderProductService orderProductService;

	public OrderController(OrderService orderService, OrderProductService orderProductService) {
		this.orderService = orderService;
		this.orderProductService = orderProductService;
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public @NotNull Iterable<Order> list() {
		return this.orderService.getAllOrders();
	}

	@SuppressWarnings("unused")
	private Iterable<Order> list_Fallback() {
		System.out.println("Order Service is down!!! fallback route enabled...");
		return null;
	}

	@PostMapping("/status/{orderId}/{status}")
	public String updateStatus(@PathVariable String orderId, @PathVariable String status) {
		orderService.updateStatus(Long.valueOf(orderId), OrderStatus.valueOf(status));
		return "OK";
	}

	@SuppressWarnings("unused")
	private String updateStatus_Fallback(String orderId, String status) {
		return "CIRCUIT BREAKER ENABLED!!! No Response From Order Service at this moment. "
				+ " Service will be back shortly - " + new Date();
	}

	@PostMapping
	public ResponseEntity<Order> create(@RequestBody OrderForm form) throws InterruptedException {
		List<OrderProductDto> formDtos = form.getProductOrders();
		validateProductsExistence(formDtos);
		String billingStatus = getBillingStatus(formDtos);
		HttpHeaders headers = new HttpHeaders();
		Order order = new Order();

		order.setStatus(OrderStatus.PENDING.name());
		order = this.orderService.create(order);

		List<OrderProduct> orderProducts = new ArrayList<>();
		for (OrderProductDto dto : formDtos) {
			orderProducts
					.add(orderProductService.create(new OrderProduct(order, dto.getProductId(), dto.getQuantity())));
		}

		order.setOrderProducts(orderProducts);

		this.orderService.update(order);

		order.getOrderProducts().forEach(p -> {
			Product temp = getById(p.getPk().getProduct_id());
			temp.setQuantity(temp.getQuantity() - p.getQuantity());
			System.out.println();
			restTemplate.put("http://localhost:8081/api/products", temp);
		});

		if (billingStatus.equals(OrderStatus.PAID.toString())) {

			order.setStatus(OrderStatus.PAID.name());

			this.orderService.update(order);

			order.setTotalOrderPrice(order.getOrderProducts().stream()
					.map(p -> getById(p.getPk().getProduct_id()).getPrice().intValue() * p.getQuantity())
					.reduce(0, Integer::sum));

			String uri = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/orders/{id}")
					.buildAndExpand(order.getId()).toString();

			headers.add("Location", uri);

			HttpHeaders restheaders = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<String> entity = new HttpEntity<String>(restheaders);

			restTemplate.exchange("http://localhost:8084/kafkaProducer/" + order.getId().toString(), HttpMethod.GET,
					entity, String.class);

			return new ResponseEntity<>(order, headers, HttpStatus.CREATED);
		} else {
			order.setStatus(OrderStatus.PAYMENTFAILED.name());
			this.orderService.update(order);
			Thread.sleep(5000);
			order.getOrderProducts().forEach(p -> {
				Product temp = getById(p.getPk().getProduct_id());
				temp.setQuantity(temp.getQuantity() + p.getQuantity());
				restTemplate.put("http://localhost:8081/api/products", temp);
			});
		}
		order.setStatus(billingStatus);
		return new ResponseEntity<>(order, headers, HttpStatus.BAD_REQUEST);
	}

	private void validateProductsExistence(List<OrderProductDto> orderProducts) {
		List<OrderProductDto> list = orderProducts.stream().filter(op -> Objects.isNull(getById(op.getProductId())))
				.collect(Collectors.toList());

		if (!CollectionUtils.isEmpty(list)) {
			new ResourceNotFoundException("Product not found");
		}
	}

	public void updateProducts(Product p) {

		System.out.println(p);

	}

	public Product getById(Long id) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		Product pro = (Product) restTemplate
				.exchange("http://localhost:8081/api/products/" + id, HttpMethod.GET, entity, Product.class).getBody();
		return pro;
	}

	public String getBillingStatus(List<OrderProductDto> formDtos) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity entity = new HttpEntity(headers);

		String billingStatus = (String) restTemplate
				.exchange("http://localhost:8085/api/billing/" + formDtos.size(), HttpMethod.GET, entity, String.class)
				.getBody();

		// String
		// billingStatus=(String)restTemplate.postForObject("http://localhost:8084/api/billing/billingStatus",
		// formDtos, String.class);
		return billingStatus;
	}

	public static class OrderForm {

		private List<OrderProductDto> productOrders;

		public List<OrderProductDto> getProductOrders() {
			return productOrders;
		}

		public void setProductOrders(List<OrderProductDto> productOrders) {
			this.productOrders = productOrders;
		}
	}
}
