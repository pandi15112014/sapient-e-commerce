package com.sapient.ecommerce.service;

import com.sapient.ecommerce.model.Order;
import com.sapient.ecommerce.model.OrderStatus;
import com.sapient.ecommerce.repository.OrderRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	private OrderRepository orderRepository;

	public OrderServiceImpl(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	@Override
	public Iterable<Order> getAllOrders() {
		return this.orderRepository.findAll();
	}

	@Override
	public Order create(Order order) {
		order.setDateCreated(LocalDate.now());

		return this.orderRepository.save(order);
	}

	@Override
	public void update(Order order) {
		this.orderRepository.save(order);
	}

	@Override
	public void updateStatus(Long orderId, OrderStatus valueOf) {

		Order order = orderRepository.findById(orderId).get();
		order.setStatus(valueOf.toString());
		orderRepository.save(order);

	}

}
