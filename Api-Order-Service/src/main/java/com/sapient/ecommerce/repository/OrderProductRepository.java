package com.sapient.ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

import com.sapient.ecommerce.model.OrderProduct;
import com.sapient.ecommerce.model.OrderProductPK;

public interface OrderProductRepository extends CrudRepository<OrderProduct, OrderProductPK> {
}
