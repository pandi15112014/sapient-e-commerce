package com.sapient.ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

import com.sapient.ecommerce.model.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
