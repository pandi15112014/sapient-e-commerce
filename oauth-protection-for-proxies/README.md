## ApiGee's OAuth2 Protection
Apigee services here show how apigee can be used to protect your REST APIs; by adding an OAuth2 Authorization layer. 

GetStarted & GetFinished are two apigee proxies; each pointing to a target. OAuthTokenGen proxy generates a token given a clien_id & secret. 

---

**Create**

1. To create the three proxies, create dummy proxies with the same name. Then in each each, select *Project* -> *Upload a New Revision...*. (May need to zip the folder first).
2. Package the GetStarted proxy as a *API Product* (under the *publish* menu). 
3. Finally create an *App* to consume the product, thus generating a client_id & secret to do so. 

---

**Test**

1. Repalce *Key* & *Secret* from the Credentials section under the newly created app. 
	- `curl -u Key:Secret https://vineetrcapigee-eval-test.apigee.net/oauth/accesstoken -d 'grant_type=client_credentials'`
2. Replace access_token with value obtained from step 1.
	- `curl -H "Authorization: Bearer access_token" https://vineetrcapigee-eval-test.apigee.net/getstarted`
	- `curl -H "Authorization: Bearer access_token" http://vineetrcapigee-eval-test.apigee.net/getfinished`

GetStarted should produce a valid XML: 

```<?xml version="1.0" encoding="UTF-8"?>
<root>
	<city>San Jose</city>
	<firstName>John</firstName>
	<lastName>Doe</lastName>
	<state>CA</state>
</root>``` 

While GetFinished should produce an error: 

```{
	"fault": {
		"faultstring":"Invalid API call as no apiproduct match found",
		"detail":{
			"errorcode":"keymanagement.service.InvalidAPICallAsNoApiProductMatchFound"
		}
	}
}```

The error occurs since GetFinished isn't part of the product, thus access to it isn't granted to the app. If added to product & token re-generated, then the same XML response as GetStarted should come. 