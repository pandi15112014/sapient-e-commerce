AWSTemplateFormatVersion: 2010-09-09
Resources:
  IAMEureka:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: ecomm-eureka-role
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: ec2.amazonaws.com
            Action: 'sts:AssumeRole'
      Policies:
        - PolicyName: eurekaPermissions
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Sid: Eureka
                Action:
                  - 'ec2:DescribeAddresses'
                  - 'ec2:AssociateAddress'
                  - 'ec2:DisassociateAddress'
                  - 'autoscaling:DescribeAutoScalingGroups'
                Effect: Allow
                Resource: '*'
  EurekaInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties: 
      InstanceProfileName: ecomm-eureka-instance-profile
      Roles: 
        - !Ref IAMEureka
  EC2Eureka:
    Type: 'AWS::EC2::Instance'
    Metadata:
      'AWS::CloudFormation::Init':
        configSets:
          InstallAndRun:
            - Install
        Install:
          packages:
            yum:
              java-1.8.0-openjdk: []
    Properties:
      IamInstanceProfile: !Ref EurekaInstanceProfile
      ImageId: ami-00068cd7555f543d5
      InstanceType: t2.micro
      KeyName: !Ref KeyPairParameter
      SecurityGroups: 
        - !Ref AllowPublic
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            DeleteOnTermination: 'true'
            Encrypted: 'true'
            VolumeSize: '8'
      UserData: 
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            yum update -y aws-cfn-bootstrap
            /opt/aws/bin/cfn-init -v \
              --stack ${AWS::StackName} \
              --resource EC2Eureka \
              --configsets InstallAndRun \
              --region ${AWS::Region}
            aws s3api get-object --bucket ${BucketNameParameter} --key ${EurekaAppParameter} ~/eureka.jar
            java -jar -Dspring.profiles.active=default,aws -Dlogging.file=/tmp/eureka.log ~/eureka.jar &
            /opt/aws/bin/cfn-signal -e $? \
              --stack ${AWS::StackName} \
              --resource EC2Eureka \
              --region ${AWS::Region}
      Tags:
        - Key: Name
          Value: ecomm-eureka-server
    CreationPolicy:
      ResourceSignal:    
        Count: 1
        Timeout: PT10M
  AllowPublic:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Allows all public connections
      GroupName: ecomm-allow-public
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 0
          ToPort: 65535
          CidrIp: 0.0.0.0/0
  Domains:
    Type: AWS::Route53::HostedZone
    Properties: 
      Name: sap-ecomm.com
      VPCs: 
        - VPCId: !Ref VPCIdParameter
          VPCRegion: us-east-1
  RegionalR53Record:
    Type: AWS::Route53::RecordSet
    Properties: 
      Type: TXT
      Name: txt.us-east-1.sap-ecomm.com
      HostedZoneId: !Ref Domains
      ResourceRecords: 
        - "\"us-east-1a.sap-ecomm.com\""
      TTL: "60"
  ZonalR53Record:
    Type: AWS::Route53::RecordSet
    Properties: 
      Type: TXT
      Name: txt.us-east-1a.sap-ecomm.com
      HostedZoneId: !Ref Domains
      ResourceRecords: 
        - !Sub "\"${EurekaHostNameParameter}\""
      TTL: "60"
  ResourcesBucketPolicy: 
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref BucketNameParameter
      PolicyDocument: 
        Statement: 
          - Action: 's3:GetObject'
            Effect: Allow
            Resource: !Join
              - ''
              - - 'arn:aws:s3:::'
                - !Ref BucketNameParameter
                - '/*'
            Principal:
              AWS: 
                - !GetAtt IAMEureka.Arn
                - !GetAtt IAMAppRole.Arn
  IAMAppRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: ecomm-app-role
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: ec2.amazonaws.com
            Action: 'sts:AssumeRole'
  AppInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties: 
      InstanceProfileName: ecomm-app-instance-profile
      Roles: 
        - !Ref IAMAppRole
  EC2ProductService:
    Type: 'AWS::EC2::Instance'
    Metadata:
      'AWS::CloudFormation::Init':
        configSets:
          InstallAndRun:
            - Install
        Install:
          packages:
            yum:
              java-1.8.0-openjdk: []
    Properties:
      IamInstanceProfile: !Ref AppInstanceProfile
      ImageId: ami-00068cd7555f543d5
      InstanceType: t2.micro
      KeyName: !Ref KeyPairParameter
      SecurityGroups: 
        - !Ref AllowPublic
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            DeleteOnTermination: 'true'
            Encrypted: 'true'
            VolumeSize: '8'
      UserData: 
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            yum update -y aws-cfn-bootstrap
            /opt/aws/bin/cfn-init -v \
              --stack ${AWS::StackName} \
              --resource EC2ProductService \
              --configsets InstallAndRun \
              --region ${AWS::Region}
            aws s3api get-object --bucket ${BucketNameParameter} --key ${ProductAppParameter} ~/prodcut-service.jar
            java -jar -Dspring.profiles.active=default,aws -Dlogging.file=/tmp/prodcut-service.log ~/prodcut-service.jar &
            /opt/aws/bin/cfn-signal -e $? \
              --stack ${AWS::StackName} \
              --resource EC2ProductService \
              --region ${AWS::Region}
      Tags:
        - Key: Name
          Value: ecomm-product-server
    CreationPolicy:
      ResourceSignal:    
        Count: 1
        Timeout: PT10M
Parameters:
  KeyPairParameter:
    Type: String
    Description: Key-Pair name used to SSH into EC2s
    Default: vineet-key-pair
  VPCIdParameter:
    Type: String
    Description: VPC Id for which R53 caters to
    Default: vpc-e0df5786
  BucketNameParameter:
    Type: String
    Description: Bucket where jars are located. Bucket Policy will be updated with created IAM Roles.
    Default: vineet-resources-us-east-1
  EurekaAppParameter:
    Type: String
    Description: Key of Eureka App in bucket
    Default: Api-EurekaServer-0.0.1-SNAPSHOT.jar
  EurekaHostNameParameter:
    Type: String
    Description: What the public DNS of Eureka Server should be
    Default: "ec2-54-156-19-152.compute-1.amazonaws.com"
  ProductAppParameter:
    Type: String
    Description: Key of Product App in bucket
    Default: Api-Product-Service-1.0.jar