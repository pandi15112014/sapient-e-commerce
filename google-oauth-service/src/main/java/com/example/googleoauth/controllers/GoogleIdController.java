package com.example.googleoauth.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.googleoauth.models.GoogleIdentity;

@RestController
public class GoogleIdController {
	private final Logger logger = LoggerFactory.getLogger(GoogleIdController.class);

	@Value("${google.userinfo.url}")
	private String userInfoUrl;
	@Autowired
	private RestTemplate template;

	@GetMapping
	public GoogleIdentity getIdentity(@RequestHeader("Authorization") String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		GoogleIdentity identity = template.exchange(userInfoUrl, HttpMethod.GET, new HttpEntity<String>(headers), GoogleIdentity.class).getBody();
		logger.info(identity.toString());
		return identity;
	}
}
