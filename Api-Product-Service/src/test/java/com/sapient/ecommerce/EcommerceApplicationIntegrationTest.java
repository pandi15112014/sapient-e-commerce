package com.sapient.ecommerce;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapient.ecommerce.EcommerceApplication;
import com.sapient.ecommerce.controller.ProductController;
import com.sapient.ecommerce.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { EcommerceApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EcommerceApplicationIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	@Autowired
	private ProductController productController;

	@Test
	public void contextLoads() {
		Assertions.assertThat(productController).isNotNull();
	}

	@Test
	public void givenGetProductsApiCall_whenProductListRetrieved_thenSizeMatchAndListContainsProductNames() {
		ResponseEntity<Iterable<Product>> responseEntity = restTemplate.exchange(
				"http://localhost:" + port + "/api/products", HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Product>>() {
				});
		Iterable<Product> products = responseEntity.getBody();
		Assertions.assertThat(products).hasSize(7);

		assertThat(products, hasItem(hasProperty("name", is("TV Set"))));
		assertThat(products, hasItem(hasProperty("name", is("Game Console"))));
		assertThat(products, hasItem(hasProperty("name", is("Sofa"))));
		assertThat(products, hasItem(hasProperty("name", is("Icecream"))));
		assertThat(products, hasItem(hasProperty("name", is("Beer"))));
		assertThat(products, hasItem(hasProperty("name", is("Phone"))));
		assertThat(products, hasItem(hasProperty("name", is("Watch"))));
	}

}
