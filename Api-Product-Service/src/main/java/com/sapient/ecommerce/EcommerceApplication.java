package com.sapient.ecommerce;

import com.sapient.ecommerce.model.Product;
import com.sapient.ecommerce.service.ProductService;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Random;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableEurekaClient
@Configuration
@EnableSwagger2
@EnableCircuitBreaker
public class EcommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceApplication.class, args);
    }
    
    //http://localhost:8081/swagger-ui.html
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }

    @Bean
    CommandLineRunner runner(ProductService productService) {
        return args -> {
        	productService.save(new Product(1L, "TV Set", 300.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(2L, "Game Console", 200.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(3L, "Sofa", 100.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(4L, "Icecream", 5.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(5L, "Beer", 3.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(6L, "Phone", 500.00,10, "http://placehold.it/200x100"));
            productService.save(new Product(7L, "Watch", 30.00,10, "http://placehold.it/200x100"));
        };
    }
}
