package com.sapient.ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

import com.sapient.ecommerce.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
