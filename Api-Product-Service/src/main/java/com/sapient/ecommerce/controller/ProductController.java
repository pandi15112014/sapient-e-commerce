package com.sapient.ecommerce.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.sapient.ecommerce.model.Product;
import com.sapient.ecommerce.service.ProductService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = { "", "/" })
    @HystrixCommand(fallbackMethod = "getProducts_Fallback", commandProperties = {
    		   @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")})
    public @NotNull Iterable<Product> getProducts() throws InterruptedException {
        return productService.getAllProducts();
    }
    
    @SuppressWarnings("unused")
    private Iterable<Product> getProducts_Fallback() {
        System.out.println("Product Service is down!!! fallback route enabled...");
		return null;      
       
    }
    
    @PutMapping(value = { "", "/" })
    public Product getProducts(@RequestBody Product product) {
        return productService.save(product);
    }
    
    @GetMapping(value = { "/{productId}" })
    public Product getProducts(@PathVariable Long productId) {
        return productService.getProductById(productId);
    }
    
    @HystrixCommand(fallbackMethod = "fallback_hello", commandProperties = {
    		   @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    		})
    @RequestMapping(value = "/hello")
    public String hello() throws InterruptedException {
       Thread.sleep(3000);
       return "Welcome Hystrix";
    }
    @SuppressWarnings("unused")
	private String fallback_hello() {
	   return "Request fails. It takes long time to response";
	}
}

