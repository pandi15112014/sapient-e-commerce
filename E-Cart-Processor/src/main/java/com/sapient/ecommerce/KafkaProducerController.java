package com.sapient.ecommerce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaProducerController {

	final private String TOPIC = "test";

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@RequestMapping("/kafkaProducer/{message}")
	public String publishMessage(@PathVariable String message) throws InterruptedException {
		kafkaTemplate.send(TOPIC, message);
		return "OK";
	}

}
