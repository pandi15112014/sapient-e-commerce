package com.sapient.ecommerce;

import java.util.Arrays;
import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class KafkaECartApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaECartApplication.class, args);
	}

	@KafkaListener(topics = "test", groupId = "eCart")
	public void listen(String message) throws InterruptedException {
		RestTemplate rt = new RestTemplate();
		HttpHeaders restheaders = new HttpHeaders();
		restheaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(restheaders);

		System.out.println("Received Messasge with order id: " + message + " with PAID Staus");

		String id = message.replaceAll("\"", "");
		Thread.sleep(new Random().nextInt(5000 - 1000) + 1000);
		rt.exchange("http://localhost:8082/api/orders/status/" + id + "/READY_FOR_DELIVERY", HttpMethod.POST, entity,
				String.class);
		System.out.println("Order id: " + message + " changed to READY_FOR_DELIVERY");
		Thread.sleep(new Random().nextInt(5000 - 1000) + 1000);
		rt.exchange("http://localhost:8082/api/orders/status/" + id + "/DELIVERED", HttpMethod.POST, entity,
				String.class);
		System.out.println("Order id: " + message + " is DELIVERED");
	}

}
