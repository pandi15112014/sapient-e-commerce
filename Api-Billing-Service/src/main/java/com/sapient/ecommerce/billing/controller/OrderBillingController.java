package com.sapient.ecommerce.billing.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.ecommerce.billing.dto.OrderStatus;

@RestController
@RequestMapping("/api/billing")
public class OrderBillingController {
	
	
	
	@GetMapping(value = "/{dtosSize}")
	public String getBillingStatus(@PathVariable int dtosSize) {
		if (dtosSize > 3) {
			return OrderStatus.FAILED.toString();
		}
		return OrderStatus.PAID.toString();
	}
	
	
}
