package com.sapient.ecommerce.billing.dto;

public enum OrderStatus {
    PAID,
    FAILED,
    READY_FOR_DELIVERY,
    DELIVERED
}
