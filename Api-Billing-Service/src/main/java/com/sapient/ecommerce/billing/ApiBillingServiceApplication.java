package com.sapient.ecommerce.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApiBillingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiBillingServiceApplication.class, args);
	}

}
