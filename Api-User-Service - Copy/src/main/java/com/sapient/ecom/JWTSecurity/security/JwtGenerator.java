package com.sapient.ecom.JWTSecurity.security;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sapient.ecom.JWTSecurity.model.JwtUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtGenerator {

	@Value("${spring.ems.secretkey}")
	private String secret;

	@Value("${spring.ems.jwt-expiry}")
	private String jwtExpiry;

	public String generate(JwtUser jwtUser) {
		// TODO Auto-generated method stub

		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, jwtUser.getUserName());

	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {

		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + Long.valueOf(jwtExpiry) * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

}
