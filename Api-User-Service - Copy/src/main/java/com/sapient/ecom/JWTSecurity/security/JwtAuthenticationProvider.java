package com.sapient.ecom.JWTSecurity.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.sapient.ecom.JWTSecurity.dao.UserRepository;
import com.sapient.ecom.JWTSecurity.model.JwtAuthnticationToken;
import com.sapient.ecom.JWTSecurity.model.JwtUser;
import com.sapient.ecom.JWTSecurity.model.JwtUserDetails;

@Service
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	JwtValidator validator;
	
	@Autowired
	UserRepository userRepository;
	
	

	@Override
	public boolean supports(Class<?> aClass) {
		// TODO Auto-generated method stub
		return (JwtAuthnticationToken.class.isAssignableFrom(aClass));
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
	
		JwtAuthnticationToken jwttoken=(JwtAuthnticationToken)authentication;
		String token=jwttoken.getToken();
		
		
		if(!validator.validateToken(token)) {
			throw new RuntimeException("Token is not valid");
		}
		
		List<GrantedAuthority> ls=AuthorityUtils.commaSeparatedStringToAuthorityList(jwttoken.getName());
		return new JwtUserDetails(jwttoken.getName(),token,ls);
	}

}
