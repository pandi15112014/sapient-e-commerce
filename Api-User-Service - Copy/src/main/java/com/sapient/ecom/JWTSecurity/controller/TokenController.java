package com.sapient.ecom.JWTSecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.ecom.JWTSecurity.domain.User;
import com.sapient.ecom.JWTSecurity.model.JwtUser;
import com.sapient.ecom.JWTSecurity.security.JwtGenerator;
import com.sapient.ecom.JWTSecurity.service.UserService;

@RestController
@RequestMapping("token")
public class TokenController {
	
	@Autowired
	JwtGenerator jwtGenerator;
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "/genrate")
	public String genrate(@RequestBody JwtUser jwtUser) {
		
		return jwtGenerator.generate(jwtUser);
		
	}
	
	@PostMapping(value = "/register")
    public ResponseEntity<?> addUser(@RequestBody User user) throws Exception {
        User savedUser =userService.addUser(user);
        return ResponseEntity.ok(savedUser);
    }
	

	
	

}
