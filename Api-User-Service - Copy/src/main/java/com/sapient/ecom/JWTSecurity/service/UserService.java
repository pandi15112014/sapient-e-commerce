package com.sapient.ecom.JWTSecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sapient.ecom.JWTSecurity.dao.UserRepository;
import com.sapient.ecom.JWTSecurity.domain.User;
import com.sapient.ecom.JWTSecurity.model.JwtUser;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	public User getUser(String userName) {
		User user= userRepository.findByName(userName);
		if (user == null) 
			throw new UsernameNotFoundException("User not found with username: " + userName);
		return user;
	}
	
	public User addUser(User user) {
		return userRepository.save(user);
	}
}


