package com.sapient.ecom.JWTSecurity.config;

import java.util.Collections;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sapient.ecom.JWTSecurity.security.JwtAuthenticationProvider;
import com.sapient.ecom.JWTSecurity.security.JwtAuthenticationTokenFilter;
import com.sapient.ecom.JWTSecurity.security.JwtSuccessHandler;
import com.sapient.ecom.JWTSecurity.security.JwtauthenticationEntryPoint;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class JWTSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JwtAuthenticationProvider jwtAuthenticationProvider;
	@Autowired
	JwtauthenticationEntryPoint entryPoint;

	@Bean
	public JwtAuthenticationTokenFilter authenticationTokenFilter() {

		JwtAuthenticationTokenFilter filter = new JwtAuthenticationTokenFilter();

		filter.setAuthenticationManager(authenticationManager());
		filter.setAuthenticationSuccessHandler(new JwtSuccessHandler());
		return filter;
	}

	@Bean
	public AuthenticationManager authenticationManager() {
		return new ProviderManager(Collections.singletonList(jwtAuthenticationProvider));
	}

	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
				.antMatchers("/token/**")
		        .permitAll();
		/*
		 * .antMatchers("/**") .authenticated();
		 */

		http.addFilterBefore((Filter) authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		http.headers().cacheControl();

	}
}
