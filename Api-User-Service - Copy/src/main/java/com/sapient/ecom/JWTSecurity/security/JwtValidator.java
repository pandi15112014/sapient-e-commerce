package com.sapient.ecom.JWTSecurity.security;

import java.util.Date;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sapient.ecom.JWTSecurity.domain.User;
import com.sapient.ecom.JWTSecurity.model.JwtUser;
import com.sapient.ecom.JWTSecurity.service.UserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtValidator {

	@Value("${spring.ems.secretkey}")
	private String secret;
	
	@Autowired
	private UserService userService;

	// retrieve username from jwt token
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	// for retrieveing any information from token we will need the secret key
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}
	
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}
	
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public Boolean validateToken(String token) {
		final String userNamefromToken = getUsernameFromToken(token);
		final User userNamefromDb=userService.getUser(userNamefromToken);
		return (userNamefromToken.equals(userNamefromDb.getName()) && !isTokenExpired(token));
	
	}

	

}
